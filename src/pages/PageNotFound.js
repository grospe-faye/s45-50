import {Row, Col} from 'react-bootstrap'

export default function PageNotFound(){
	return (
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the homepage</p>
			</Col>
		</Row>
	)
}

/* Instructor's solution
import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}

*/