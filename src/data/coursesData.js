const coursesData = [
	{
		id:"wdc0001",
		name: "PHP - Laravel",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus modi totam, veniam enim, assumenda unde vero id, doloribus, asperiores animi commodi! Veritatis tempora sed facilis earum sint corrupti ab at.",
		price: 45000,
		onOffer: true
	},
	{
		id:"wdc0002",
		name: "Python - Django",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus modi totam, veniam enim, assumenda unde vero id, doloribus, asperiores animi commodi! Veritatis tempora sed facilis earum sint corrupti ab at.",
		price: 55000,
		onOffer: true
	},
	{
		id:"wdc0003",
		name: "Java - Springboot",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus modi totam, veniam enim, assumenda unde vero id, doloribus, asperiores animi commodi! Veritatis tempora sed facilis earum sint corrupti ab at.",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;